package main

import (
	"fmt"
	"os"
)

func main() {
	var (
		user    string
		homeDir string
	)

	user = os.Getenv("USER")
	homeDir = os.Getenv("HOME")

	fmt.Printf("Hello %s", user)
	fmt.Printf("\nHome anda  di %s", homeDir)
	fmt.Printf("\n")
}
